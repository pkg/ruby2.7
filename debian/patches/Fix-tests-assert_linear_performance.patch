Origin: https://github.com/ruby/ruby/commit/6fa7e5e05b5f62cd97af0f35627377c053d552af
Reviewed-by: Sylvain Beucler <beuc@debian.org>
Last-Update: 2024-08-03

From 6fa7e5e05b5f62cd97af0f35627377c053d552af Mon Sep 17 00:00:00 2001
From: Hiroshi SHIBATA <hsbt@ruby-lang.org>
Date: Tue, 28 Mar 2023 18:00:13 +0900
Subject: Added assert_linear_performance for URI tests

---
 tool/lib/test/unit/core_assertions.rb | 33 +++++++++++++++++++++++++++
 1 file changed, 33 insertions(+)

Index: ruby/tool/lib/test/unit/core_assertions.rb
===================================================================
--- ruby.orig/tool/lib/test/unit/core_assertions.rb
+++ ruby/tool/lib/test/unit/core_assertions.rb
@@ -398,6 +398,39 @@ eom
       end
       alias all_assertions assert_all_assertions
 
+      # Expect +seq+ to respond to +first+ and +each+ methods, e.g.,
+      # Array, Range, Enumerator::ArithmeticSequence and other
+      # Enumerable-s, and each elements should be size factors.
+      #
+      # :yield: each elements of +seq+.
+      def assert_linear_performance(seq, rehearsal: nil, pre: ->(n) {n})
+        first = seq.first
+        *arg = pre.call(first)
+        times = (0..(rehearsal || (2 * first))).map do
+          st = Process.clock_gettime(Process::CLOCK_MONOTONIC)
+          yield(*arg)
+          t = (Process.clock_gettime(Process::CLOCK_MONOTONIC) - st)
+          assert_operator 0, :<=, t
+          t.nonzero?
+        end
+        times.compact!
+        tmin, tmax = times.minmax
+        tmax *= tmax / tmin
+        tmax = 10**Math.log10(tmax).ceil
+
+        seq.each do |i|
+          next if i == first
+          t = tmax * i.fdiv(first)
+          *arg = pre.call(i)
+          message = "[#{i}]: in #{t}s"
+          Timeout.timeout(t, Timeout::Error, message) do
+            st = Process.clock_gettime(Process::CLOCK_MONOTONIC)
+            yield(*arg)
+            assert_operator (Process.clock_gettime(Process::CLOCK_MONOTONIC) - st), :<=, t, message
+          end
+        end
+      end
+
       def message(msg = nil, *args, &default) # :nodoc:
         if Proc === msg
           super(nil, *args) do
